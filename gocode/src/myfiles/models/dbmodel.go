package models

import (
	"github.com/dgrijalva/jwt-go"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	//MongoDatabase pointer to db
	MongoDatabase *MongoDB
)

//MongoDB contains Database and collections string
type MongoDB struct {
	DB         *mgo.Database
	Collection string
}

//TokensPair refreshStruct
type TokensPair struct {
	RefreshToken string `json:"RefreshToken" bson:"RefreshToken"`
	AccessToken  string `json:"AccessToken" bson:"AccessToken"`
}

//Token struct
type Token struct {
	UserID     string       `json:"id" bson:"_id"`
	TokenPairs []TokensPair `json:"TokenPairs" bson:"TokenPairs"`
}

//Claims ds
type Claims struct {
	UserID string `json:"id" bson:"_id"`
	jwt.StandardClaims
}

//Tokens array
type Tokens []Token

//Connect to database
func Connect() (*mgo.Database, error) {
	host := "mongodb://localhost:27017"
	dbName := "account_golang"
	session, err := mgo.Dial(host)
	if err != nil {
		return nil, err
	}
	db := session.DB(dbName)
	return db, nil

}

//GetMongoDBStruck return MongoDB struct
func GetMongoDBStruck(db *mgo.Database) *MongoDB {
	return &MongoDB{DB: db, Collection: "tokens"}
}

//DatabaseController makes actions upon DB
type DatabaseController interface {
	Save(data *Token) error
	Update(filter interface{}, query interface{}) error
	UpdateAll(filter interface{}, query interface{}) (*mgo.ChangeInfo, error)
	Delete(filter interface{}) error
	DeleteAll(filter interface{}) (*mgo.ChangeInfo, error)
	FindByID(id string) (*Token, error)
	FindAll() (*Tokens, error)
	Find(filter interface{}) (*Tokens, error)
	FindPair(filter interface{}, refreshToken string) (*TokensPair, error)
}

//Save to DB
func (mongo *MongoDB) Save(data *Token) error {
	return mongo.DB.C(mongo.Collection).Insert(data)
}

//Update data at given index in DB
func (mongo *MongoDB) Update(filter interface{}, query interface{}) error {
	return mongo.DB.C(mongo.Collection).Update(filter, query)
}

//UpdateAll ///
func (mongo *MongoDB) UpdateAll(filter interface{}, query interface{}) (*mgo.ChangeInfo, error) {
	return mongo.DB.C(mongo.Collection).UpdateAll(filter, query)
}

//Delete sertain data in DB
func (mongo *MongoDB) Delete(filter interface{}) error {
	return mongo.DB.C(mongo.Collection).Remove(filter)
}

//DeleteAll data corresponding to query
func (mongo *MongoDB) DeleteAll(filter interface{}) (*mgo.ChangeInfo, error) {
	return mongo.DB.C(mongo.Collection).RemoveAll(filter)
}

//FindByID data at given index in DB
func (mongo *MongoDB) FindByID(id string) (*Token, error) {
	var token = Token{}
	err := mongo.DB.C(mongo.Collection).FindId(id).One(&token)
	if err != nil {
		return nil, err
	}
	return &token, err
}

//FindAll data in collection
func (mongo *MongoDB) FindAll() (*Tokens, error) {
	var tokens = Tokens{}
	err := mongo.DB.C(mongo.Collection).Find(bson.M{}).All(&tokens)
	return &tokens, err
}

//Find sertain token by filter
func (mongo *MongoDB) Find(filter interface{}) (*Token, error) {
	var token = &Token{}
	err := mongo.DB.C(mongo.Collection).Find(filter).One(token)
	return token, err
}

//FindPair sertain token pair
func (mongo *MongoDB) FindPair(filter interface{}, refreshToken string) (*TokensPair, error) {
	var token = &Token{}
	err := mongo.DB.C(mongo.Collection).Find(filter).One(token)
	if err != nil {
		return nil, err
	}
	for _, item := range token.TokenPairs {
		if item.RefreshToken == refreshToken {
			return &item, nil
		}
	}
	return nil, nil
}
