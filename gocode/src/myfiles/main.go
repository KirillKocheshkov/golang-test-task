package main

import (
	"fmt"
	"log"
	"myfiles/handlers"
	"myfiles/models"
	"myfiles/routs"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {

	log.Println("Routes:\n" + "http://localhost:8000/" + "______ return Collection's documents \n" +
		"http://localhost:8000/" + routs.LOGINROUT + "______  Get Access/RefreshToken" + "\n" +
		"http://localhost:8000" + routs.REFRESHROUT + "______ Refresh tokens pairs" + "\n" +
		"http://localhost:8000" + routs.DELETEREFRESHROUT + "______ Delete All Tokens" + "\n" +
		"http://localhost:8000" + routs.DELETEREFRESHONEROUT + "______ Delete One Token" + "\n")

	router := mux.NewRouter()
	db, err := models.Connect()
	if err != nil {
		fmt.Println(err)
	} else {
		models.MongoDatabase = &models.MongoDB{DB: db, Collection: "tokens"}
	}

	// RountHandlers
	router.HandleFunc(routs.LOGINROUT, handlers.HandleLogIn).Methods("GET")
	router.HandleFunc(routs.REFRESHROUT, handlers.RefreshToken).Methods("POST")
	router.HandleFunc(routs.DELETEREFRESHROUT, handlers.DeleteAllRefreshTokens).Methods("PUT")
	router.HandleFunc(routs.DELETEREFRESHONEROUT, handlers.DeleteRefreshToken).Methods("PUT")

	router.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		if models.MongoDatabase != nil {
			token, err2 := models.MongoDatabase.FindByID("53231")
			if err2 != nil {
				fmt.Fprintln(res, err2)
			} else {
				handlers.RespondWithJson(res, http.StatusOK, token)
			}
		} else {
			handlers.RespondWithError(res, http.StatusServiceUnavailable, "Can not connect to Database")
		}

	}).Methods("GET")

	http.ListenAndServe(":8000", router)

}
