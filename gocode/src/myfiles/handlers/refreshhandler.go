package handlers

import (
	"encoding/json"
	"myfiles/models"
	"myfiles/routs"

	"github.com/dgrijalva/jwt-go"
	"gopkg.in/mgo.v2/bson"

	"net/http"
)

//RefreshStruct refreshStruct

//RefreshToken refreshes token
func RefreshToken(res http.ResponseWriter, req *http.Request) {
	var refreshTokenStruck models.TokensPair
	var refreshclaims = &models.Claims{}
	var accessclaims = &models.Claims{}
	err := json.NewDecoder(req.Body).Decode(&refreshTokenStruck)
	if err != nil {
		RespondWithJson(res, http.StatusExpectationFailed, err.Error()+" decode")
	}

	//Cheking that token has expired
	_, accesserror := jwt.ParseWithClaims(refreshTokenStruck.AccessToken, accessclaims, func(token *jwt.Token) (interface{}, error) {
		return []byte(routs.SECRETKEY), nil
	})

	if accesserror != nil {

		RespondWithError(res, http.StatusBadRequest, accesserror.Error()+" some stuff")
		return
	}

	//get refresh token
	refreshtok, refresherror := jwt.ParseWithClaims(refreshTokenStruck.RefreshToken, refreshclaims, func(token *jwt.Token) (interface{}, error) {
		return []byte(routs.SECRETKEYRefresh), nil
	})

	if refresherror != nil {
		RespondWithJson(res, http.StatusNotAcceptable, refresherror.Error()+" refresh token"+"   "+refreshTokenStruck.RefreshToken)
	}

	if !refreshtok.Valid {
		RespondWithError(res, http.StatusUnauthorized, "Refresh token is not valid.")
		return
	}
	// tries to find pair

	foundToken, err := models.MongoDatabase.FindPair(bson.M{"TokenPairs.AccessToken": refreshTokenStruck.AccessToken}, refreshTokenStruck.RefreshToken)
	if err != nil {
		RespondWithError(res, http.StatusUnauthorized, err.Error())
		return
	}

	if foundToken == nil {
		RespondWithError(res, http.StatusUnauthorized, "Can't find refresh token")
		return
	}

	// refresh token is valid, get new pair of tokens
	newTokenPair, fail := GetRefreshAndAccessTokens(res, accessclaims.UserID)
	if fail != nil {
		RespondWithError(res, http.StatusConflict, "Can't get new tokens pair")
		return
	}
	RespondWithJson(res, http.StatusOK, newTokenPair.TokenPairs)

	//remove old token pair
	uperre := models.MongoDatabase.Update(bson.M{"_id": refreshclaims.UserID}, bson.M{"$pull": bson.M{
		"RefreshToken": refreshTokenStruck.RefreshToken,
		"AccessToken":  refreshTokenStruck.AccessToken}})
	if uperre != nil {
		RespondWithError(res, http.StatusInternalServerError, uperre.Error())
	}
}
