package handlers

import (
	"encoding/json"
	"net/http"
)

//RespondWithError prints json responce
func RespondWithError(res http.ResponseWriter, code int, msg string) {
	RespondWithJson(res, code, map[string]string{"error": msg})
}

//RespondWithJson prints json responce
func RespondWithJson(res http.ResponseWriter, code int, payload interface{}) {
	responce, _ := json.Marshal(payload)
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(code)
	res.Write(responce)

}
