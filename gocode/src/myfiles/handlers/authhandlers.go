package handlers

import (
	"myfiles/models"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

//HandleLogIn return access and refresh tokens
func HandleLogIn(res http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	token, err := GetRefreshAndAccessTokens(res, params["id"])
	if err != nil {
		RespondWithError(res, http.StatusBadRequest, err.Error())
		return
	}

	RespondWithJson(res, http.StatusOK, token)

}

//GetRefreshAndAccessTokens returns tokens
func GetRefreshAndAccessTokens(res http.ResponseWriter, userID string) (*models.Token, error) {
	shaToken := &GUID{UserID: userID}
	locToken, err := shaToken.GetTokens()
	if err != nil {
		return nil, err
	}
	/// check if the user already exists
	_, failed := models.MongoDatabase.FindByID(userID)
	if failed != nil {
		err2 := models.MongoDatabase.Save(locToken)
		if err2 != nil {
			return nil, err
		}
		return locToken, nil

	}
	upErr := models.MongoDatabase.Update(bson.M{"_id": userID}, bson.M{"$push": bson.M{
		"TokenPairs": locToken.TokenPairs[0]}})

	if upErr != nil {
		return nil, upErr
	}

	return locToken, nil
}
