package handlers

import (
	"myfiles/models"
	"myfiles/routs"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

//GUID struct for user
type GUID struct {
	UserID string `json:"id" bson:"_id"`
}

//GetTokens return access token
func (guid GUID) GetTokens() (*models.Token, error) {
	var token = models.Token{TokenPairs: make([]models.TokensPair, 0)} // make tokenpair struct
	accesstoken := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.MapClaims{
		"id":  guid.UserID,
		"exp": time.Now().Add(time.Minute * 15).Unix(),
	})
	signedtoken, accesserr := accesstoken.SignedString([]byte(routs.SECRETKEY))
	if accesserr != nil {
		return nil, accesserr
	}
	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.MapClaims{
		"id":  guid.UserID,
		"exp": time.Now().Add(time.Hour * 2).Unix(),
	})
	signedrefreshtoken, refresherr := refreshToken.SignedString([]byte(routs.SECRETKEYRefresh))
	if refresherr != nil {
		return nil, refresherr
	}
	var newpair = models.TokensPair{AccessToken: signedtoken, RefreshToken: signedrefreshtoken}
	token.TokenPairs = append(token.TokenPairs, newpair)
	token.UserID = guid.UserID
	return &token, nil

}

//GetTokenInterface interface to get token
type GetTokenInterface interface {
	GetTokens() (string, error)
}
