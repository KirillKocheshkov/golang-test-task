package handlers

import (
	"encoding/json"
	"myfiles/models"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

//DeleteRefreshTokenStruct struct for deletion
type DeleteRefreshTokenStruct struct {
	UserID  string `json:"id" bson:"_id"`
	Refresh string `json:"RefreshToken" bson:"RefreshToken"`
}

//DeleteAllRefreshTokens delets all pairs
func DeleteAllRefreshTokens(res http.ResponseWriter, req *http.Request) {
	var userID = GUID{}
	err := json.NewDecoder(req.Body).Decode(&userID)
	if err != nil {
		RespondWithError(res, http.StatusBadRequest, err.Error())
		return
	}
	_, err2 := models.MongoDatabase.UpdateAll(bson.M{"_id": userID.UserID}, bson.M{"$unset": bson.M{"TokenPairs": bson.M{"RefreshToken": ""}}})
	if err2 != nil {
		RespondWithError(res, http.StatusBadRequest, err.Error())
		return
	}
	RespondWithJson(res, http.StatusOK, "tokens were deleteted")
}

//DeleteRefreshToken remove one refresh token
func DeleteRefreshToken(res http.ResponseWriter, req *http.Request) {
	var deleteStruct = DeleteRefreshTokenStruct{}
	err := json.NewDecoder(req.Body).Decode(&deleteStruct)
	if err != nil {
		RespondWithError(res, http.StatusBadRequest, err.Error())
		return
	}
	err2 := models.MongoDatabase.Update(bson.M{
		"_id":                     deleteStruct.UserID,
		"TokenPairs.RefreshToken": deleteStruct.Refresh},
		bson.M{"$set": bson.M{"TokenPairs.$.RefreshToken": ""}})

	if err2 != nil {
		RespondWithError(res, http.StatusBadRequest, err.Error())
		return
	}
	RespondWithJson(res, http.StatusOK, string(deleteStruct.Refresh)+" token was deleteted")
}
