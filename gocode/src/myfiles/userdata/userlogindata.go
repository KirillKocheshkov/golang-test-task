package userdata

import "gopkg.in/mgo.v2/bson"

type UserData struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	UserName    string        `json:"name" bson:"name"`
	UserPasword string        `json:"Pasword" bson:"Pasword"`
	UserEmail   string        `json:"Email" bson:"Email"`
}
